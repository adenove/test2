import React from 'react';
import { Link } from "react-router-dom";
import { Layout, Menu, Icon } from 'antd';
const {Sider } = Layout;

const Sidebar = () => {
    return (
            <Sider width={200} style={{ background: '#fff' }}>
              <Menu defaultSelectedKeys={['1']}>
                <Menu.Item key="1">
                  <Link to="/">
                    <Icon type="audit" />
                      <span>Master Modul</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="2">
                  <Link to="/bussines">
                    <Icon type="profile" />
                    <span>Master Business</span>
                  </Link>
                </Menu.Item>
              </Menu>
            </Sider>
    )
}

export default Sidebar;