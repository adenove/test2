import React,{Component} from 'react';
import MasterModule from '../../container/masterModule/MasterModule';
import Bussines from '../../container/Bussines/Bussines';
import Sidebar from './Sidebar';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Layout } from 'antd';
const { Header, Content } = Layout;



export default class Admin extends Component {

  render() {
    return (
      <Router>
        <Layout style={{ minHeight: '100vh' }} >
          <Header className="header" style={{padding:0}} >
            <div className="logo" style={{width:200,textAlign:'center'}} >
              <h3 style={{color:'white'}} >Test 2</h3>
            </div>
          </Header>
          <Layout>
            <Sidebar/>
            <Layout style={{ padding: '20px 24px 24px' }}>
              <Content style={{ background: '#fff',padding: 24,margin: 0,minHeight: 280,}} >
                <Route path="/" exact component={MasterModule} />
                <Route path="/bussines" component={Bussines} />
              </Content>
            </Layout>
          </Layout>
        </Layout>
      </Router>
    );
  }
}
