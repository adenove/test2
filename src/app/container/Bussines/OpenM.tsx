import React from 'react';
import { Modal, Form, Input,Select } from 'antd';
const { Option } = Select;

// declare interface
interface isForm {
  visible:any;
  onCancel:any;
  onCreate:any;
  form:any;
  handleParameter:any;
  handleValue:any;
  data:any;
  wrappedComponentRef:any;
  handleSelect:any;
  val:any;
}

interface myModal {
  selectData:any;
  visible:boolean;
  oke:any;
  cancel:any;
  handleParameter:any;
  handleValue:any;
  handleSelect:any;
  val:any;
}

const CollectionCreateForm = Form.create<isForm>({ name: 'form_in_modal' })(
  class extends React.Component<isForm> {
    render() {
      const { visible, onCancel, onCreate, form,handleParameter,handleValue,data,handleSelect,val } = this.props;
      const { getFieldDecorator } = form;
      // looping data select
      let ini = data.map((item:any)=>{
          return <Option key={item.key} value={item.key}>{item.name}</Option>;
        });
      return (
        <Modal
          visible={visible}
          title="Bussines Module"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <Form.Item label="Parameter">
              {getFieldDecorator('parameter', {
                initialValue: val.param,
                rules: [{ required: true, message: 'Please input the parameter !'},{max: 15, message: 'max 15 Character' }],
              })(<Input onChange={handleParameter} />)}
            </Form.Item>
            <Form.Item label="Value">
              {getFieldDecorator('value', {
                initialValue: val.value,
              })(<Input onChange={handleValue} type="number" />)}
            </Form.Item>
            <Form.Item label="Select">
              {getFieldDecorator('select', {
                initialValue: val.select,
                rules: [{required: true, message: 'Please input the select'}],
              })(<Select onChange={handleSelect} >{ini}</Select>)}
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  },
);

class OpenM extends React.Component<myModal> {

  formRef:any;

  handleCreate = () => {
    const { form } = this.formRef.props;
    this.props.oke(form);
  };

  saveFormRef = (formRef:any) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <div>
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          data={this.props.selectData}
          visible={this.props.visible}
          onCancel={this.props.cancel}
          onCreate={this.handleCreate}
          handleParameter={this.props.handleParameter}
          handleValue={this.props.handleValue}
          handleSelect={this.props.handleSelect}
          val={this.props.val}
        />
      </div>
    );
  }
}


export default OpenM;