import React,{Component,Fragment} from 'react';
import {connect} from 'react-redux';
import OpenM from './OpenM';
import { Button,Input,Table } from 'antd';
const { Search } = Input;

interface data {
  masterDT:any;
  bussinesDT:any;
  saveBussines:any;
  editBussines:any;
  deleteBussines:any;
}

class Bussines extends Component<data> {

    state = {visible: false,status:"",parameter:"",value:0,select:"",key:"",myData:[]};

    colom = [{title: 'Parameter',dataIndex: 'parameter',key: 'parameter'},
             {title: 'Value',dataIndex: 'value',key: 'value'},
             {title: 'Master',dataIndex: 'master',key: 'master'},
             {title: 'Action',key: 'action',
                render: (text:string, record:any) => (
                  <span>
                    <Button type="primary" style={{marginRight:'10px'}} onClick={()=>this.ModalToggle('EDIT',record.key)} >Edit</Button>
                    <Button type="danger" onClick={()=> this.delete(record.key) } >Delete</Button>
                  </span>
                ),
            }];

    //filter data Bussines
    dataBussines:any;

    //function filter data bussines
    fillterData = () => {
      let array:any=[];
      this.props.bussinesDT.forEach((k:any,v:any) => {
        array.push({key:k.key,parameter:k.parameter,value:k.value,masterid:k.master,master:"data tidak ada"});
        this.props.masterDT.forEach((u:any,y:any) => {
          if(k.master === u.key){
            array[v].master = u.name;
          }
        });
      });
      this.dataBussines = array;
      return this.dataBussines;
    }

    // modal toggle handdle
    ModalToggle = (status:string = "",key:string="") => {
        let select;
        // set value select
        if(this.props.masterDT.length > 0){
          select = this.props.masterDT[0].key;
        }else{
          select = "";
        }
        this.setState({visible:!this.state.visible,status:status,key:key,parameter:"",value:0,select:select},()=>{
          if(status === "EDIT"){
            this.props.bussinesDT.forEach((k:any,v:number) => {
              if(k.key === key){
                this.setState({parameter:k.parameter,value:k.value,select:k.master});
              }
           });
          }
        });
    }

    // handle input parameter
    parameterChange = (e:any) => {
        this.setState({parameter:e.target.value});
    }

    // handle input value
    valueChange = (e:any) => {
      this.setState({value:e.target.value});
    }

    // handle select change
    selectChange = (e:any) => {
      this.setState({select:e});
    }

    // handel delete data Bussines
    delete = (key:number) => {
      this.props.deleteBussines(key);
      this.setState({status:"DELETE"});
      this.setState({myData:this.fillterData()});
    }

    // handele search
    search = (e:any) => {
      let array:any = [];
      if(e.target.value !== ""){
        this.dataBussines.forEach((k:any,v:any) => {
          if(k.parameter.search(e.target.value) > -1 || k.value.toString().search(e.target.value) > -1 || k.master.search(e.target.value) > -1 ){
            array.push(k);
          }
        });
        this.setState({myData:array});
      }else{
        this.setState({myData:this.fillterData()});
      }
    }

    // handle submit form
    ok = (form:any) => {
      form.validateFields((err:any, values:any) => {
        if (err) {
          return;
        }else{
          if(this.state.status === "ADD") {
            this.props.saveBussines({parameter:this.state.parameter,value:this.state.value,select:this.state.select});
            this.setState({myData:this.fillterData()});
          }else if(this.state.status === "EDIT"){
            this.props.editBussines({key:this.state.key,parameter:this.state.parameter,value:this.state.value,master:this.state.select});
            this.setState({myData:this.fillterData()});
          }
          form.resetFields();
          this.setState({ visible: false });
        }
      });
    }

    componentDidMount(){
      this.setState({ value:"0",myData:this.fillterData()});
      if(this.props.masterDT.length > 0) {
        this.setState({select:this.props.masterDT[0].name});
      }
    }

    render(){
        return (
                <Fragment>
                  <OpenM
                    selectData={this.props.masterDT}
                    visible={this.state.visible}
                    oke={this.ok}
                    cancel={this.ModalToggle}
                    handleParameter={this.parameterChange}
                    handleValue={this.valueChange}
                    handleSelect={this.selectChange}
                    val={{param:this.state.parameter,value:this.state.value,select:this.state.select}}
                  />
                  <div className="add" style={{display:'flex'}} >
                    <Search placeholder="search" onChange={this.search} />
                    <Button style={{marginLeft:'10px'}} type="primary" onClick={()=>this.ModalToggle("ADD")}>Add Data</Button>
                  </div>
                  <br/>
                  <div className="table">
                      <Table columns={this.colom} dataSource={this.state.myData} />
                  </div>
                </Fragment>
              )
            }
}

const storeToprop = (state:any) => {
    return {
      masterDT:state.masterModule,
      bussinesDT:state.bussines
    }
}

const propTostore = (dispatch:any) => {
    return{
      saveBussines  : (data:any) => {dispatch({type:'SAVE_BUSSINES',data:data})},
      editBussines  : (data:any) => {dispatch({type:'EDIT_BUSSINES',data:data})},
      deleteBussines: (key:number) => {dispatch({type:'DELETE_BUSSINES',key:key})}
    }
}

export default connect(storeToprop,propTostore)(Bussines);