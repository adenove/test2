import React,{Component,Fragment} from 'react';
import OpenM from './OpenM';
import {connect} from 'react-redux';
import { Button, Input,Table } from 'antd';
const { Search } = Input;

interface person {
    name:string;
    master:any;
    masterTostore:any;
    editMaster:any;
    deleteMaster:any;
  }


class MasterModule extends Component<person> {

    state = { visible:false,name:"",status:"",key:"",myData:[]}

    colom = [{title: 'Name',dataIndex: 'name',key: 'name'},
             {title: 'Action',key: 'action',
              render: (text:string, record:any) => (
                 <span>
                   <Button type="primary" style={{marginRight:'10px'}} onClick={()=>this.ModalToggle("EDIT",record.key)} >Edit</Button>
                   <Button type="danger" onClick={()=> this.delete(record.key) } >Delete</Button>
                 </span>
                ),
              }];

    //Modal Toggle
    ModalToggle = (status:string = "",value:string = "") => {
        this.setState({visible:!this.state.visible,name:"",status:status,key:value},()=>{
          if(status === "EDIT" ){
             this.props.master.forEach((k:any,v:number) => {
                if(k.key === value){
                  this.setState({name:k.name});
                }
             });
          }
        });
    }

    // handle input name
    nameInput = (e:any) => {
        this.setState({name:e.target.value});
    }

    // handle delete
    delete = (key:number) => {
      this.props.deleteMaster(key);
      this.setState({status:"DELETE"});
    }

    // handle search
    search = (e:any) => {
      let array:any = [];
      if(e.target.value !== ""){
        // array filter
        this.props.master.forEach((k:any,v:any) => {
          if(k.name.search(e.target.value) > -1 ){
            array.push(k);
          }
          this.setState({myData:array});
        });
      }else{
        // else = input == ""
        this.setState({myData:this.props.master});
      }
    }

    // handle form submit
    ok = (form:any) => {
      form.validateFields((err:any, values:any) => {
      if (err) {
        return;
      }else{
        if(this.state.status === "ADD") {
          // save to store
          this.props.masterTostore(this.state.name)
        }
        else{
          // edit to store
          this.props.editMaster(this.state.name,this.state.key);
        }
        form.resetFields();
        this.setState({ visible: false });
      }
      });
    }

    componentDidMount(){
      this.setState({myData:this.props.master});
    }

    render(){
        return (
           <Fragment>
            <OpenM
              show={this.state.visible}
              hide={this.ModalToggle}
              handleVal={this.nameInput}
              handleSave={this.ok}
              nameValue={this.state.name}
            />
            <div className="add" style={{display:'flex',marginBottom:20}} >
              <Search placeholder="search" onChange={this.search} />
              <Button style={{marginLeft:'10px'}} type="primary" onClick={()=>this.ModalToggle('ADD')}>Add Data</Button>
            </div>
            <div className="table">
              <Table columns={this.colom} dataSource={this.state.myData} />
            </div>
           </Fragment>
        )
      }
  }


const storeToprops = (state:any) => {
    return {
            master:state.masterModule
    }
}

const dispatchAction = (dispatch:any) => {
  return {
    masterTostore: (value:string) => dispatch({type:'PUSH_MASTER',name:value}),
    editMaster:(name:string,key:number) => dispatch({type:'EDIT_MASTER',data:{key:key,name:name}}),
    deleteMaster:(key:number) => dispatch({type:'DELETE_MASTER',key:key})
  }
}

export default connect(storeToprops,dispatchAction)(MasterModule);