import React from 'react';
import { Modal, Form, Input } from 'antd';

// declare interface
interface isForm {
  visible:any;
  onCancel:any;
  onCreate:any;
  form:any;
  wrappedComponentRef:any;
  handleInput:any;
  isname:string;
}

interface myModal {
  show?:boolean;
  hide?:any;
  handleVal:any;
  handleSave:any;
  nameValue:string;
}

const CollectionCreateForm = Form.create<isForm>({ name: 'form_in_modal' })(
  // eslint-disable-next-line
  class extends React.Component<isForm> {
    render() {
      const { visible, onCancel, onCreate, form,handleInput,isname } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Master Module"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form layout="vertical">
            <Form.Item label="Name">
              {getFieldDecorator('name', {
                initialValue: isname,
                rules: [{ required: true, message: 'Please input the title!'},{max: 15, message: 'max 15 Character' }],
              })(<Input onChange={handleInput} />)}
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  },
);

class OpenM extends React.Component<myModal> {

  formRef:any;

  // handle form send param to masterModule
  handleCreate = () => {
    const { form } = this.formRef.props;
    this.props.handleSave(form);

  };

  saveFormRef = (formRef:any) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <div>
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.props.show}
          onCancel={this.props.hide}
          onCreate={this.handleCreate}
          handleInput={this.props.handleVal}
          isname={this.props.nameValue}
        />
      </div>
    );
  }
}


export default OpenM;