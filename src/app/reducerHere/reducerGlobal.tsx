interface myState {
    masterModule:any[];
    bussines:any[];
}

const defaultState = {
    masterModule:[],
    bussines:[]
}

const reducerGlobal = (state:myState = defaultState,action:any) => {
    // Master Module Action
    if(action.type === "PUSH_MASTER"){
        if(state.masterModule.length === 0) {
            let master = {key:state.masterModule.length+1,name:action.name};
            state.masterModule.push(master);
        }else{
            let master = {key:state.masterModule[state.masterModule.length-1].key+1,name:action.name};
            state.masterModule.push(master);
        }
    }else if(action.type === "EDIT_MASTER"){
        state.masterModule.forEach((k:any,v:any) => {
            if(action.data.key === k.key){
                state.masterModule[v].name = action.data.name;
            }
        });
    }else if(action.type === "DELETE_MASTER"){
        state.masterModule.forEach((k,v) => {
            if(k.key === action.key){
                state.masterModule.splice(v,1);
            }
        });
    }
    // Bussines Action
    else if(action.type === "SAVE_BUSSINES"){
        if(state.bussines.length === 0) {
            let bussines = {key:(state.bussines.length+1),parameter:action.data.parameter,value:action.data.value,master:action.data.select}
            state.bussines.push(bussines);
        }else{
            let bussines = {key:state.bussines[state.bussines.length-1].key+1,parameter:action.data.parameter,value:action.data.value,master:action.data.select}
            state.bussines.push(bussines);
        }
    }else if(action.type === "EDIT_BUSSINES"){
        state.bussines.forEach((k:any,v:any) => {
            if(k.key === action.data.key){
                state.bussines[v].key = action.data.key;
                state.bussines[v].parameter = action.data.parameter;
                state.bussines[v].value = action.data.value;
                state.bussines[v].master = action.data.master;
            }
        });
    }else if(action.type === "DELETE_BUSSINES"){
        state.bussines.forEach((k,v) => {
            if(k.key === action.key){
                state.bussines.splice(v,1);
            }
        });
    }
    return state;
}

export default reducerGlobal;